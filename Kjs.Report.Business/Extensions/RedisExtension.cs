﻿using System.Collections.Generic;

namespace Kjs.Report.Business
{
    public class RedisExtension
    {
        public static readonly string REDIS_MENU_TAG = "MENU_"; //菜单redis
        public static readonly int REDIS_MENU_EXPIRATION = 2 * 60 * 60; //2小时过期

        /// <summary>
        /// 通过key集合批量清除对应的value
        /// </summary>
        /// <param name="redisTag">key标签</param>
        /// <param name="keyList">key变量集合</param>
        /// <returns></returns>
        public static long RemoveRedisByKeyList(string redisTag,List<int> keyList)
        {
            List<string> keys = new List<string>();
            foreach (var key in keyList)
            {
                keys.Add(redisTag + key);
            }
            return CSRedis.QuickHelperBase.Remove(keys.ToArray());
        }

        /// <summary>
        /// 移除redis
        /// </summary>
        /// <param name="redisTag">key标签</param>
        /// <param name="key">key变量</param>
        /// <returns></returns>
        public static long RemoveRedis(string redisTag,int key)
        {
            return CSRedis.QuickHelperBase.Remove(redisTag+key);
        }

        /// <summary>
        /// 设置redis信息
        /// </summary>
        /// <param name="redisTag">key标签</param>
        /// <param name="key">key变量</param>
        /// <param name="value">value值</param>
        /// <param name="expire">有效时间 秒</param>
        /// <returns></returns>
        public static bool SetRedis(string redisTag, string key,string value,int expire)
        {
            return CSRedis.QuickHelperBase.Set(redisTag + key, value, expire);
        }

        /// <summary>
        /// 获取redis信息
        /// </summary>
        /// <param name="redisTag">key标签</param>
        /// <param name="key">key变量</param>
        /// <returns></returns>
        public static string GetRedis(string redisTag, string key)
        {
            return CSRedis.QuickHelperBase.Get(redisTag + key);
        }
    }
}

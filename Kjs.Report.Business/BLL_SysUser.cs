﻿using AutoMapper;
using Kjs.Report.Domain.Table;
using Kjs.Report.IService;
using Kjs.Report.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Kjs.Report.Business
{
    public class BLL_SysUser
    {
        private readonly ISysUserService _userService;
        private readonly IMapper _mapper;
        private readonly ClaimsPrincipal _admin;
        private readonly string _ip;

        public BLL_SysUser(ISysUserService userService, IMapper mapper,  ClaimsPrincipal admin, string ip)
        {
            _userService = userService;
            _mapper = mapper;
            _admin = admin;
            _ip = ip;
        }

        /// <summary>
        /// 根据用户账号密码获取用户信息
        /// </summary>
        /// <returns></returns>
        public async Task<M_Sys_User> Get(string userName)
        {
            var user = await _userService.Get(userName);
            if (user != null)
            {
                var m_user = _mapper.Map<M_Sys_User>(user);
                return m_user;
            }
            return null;
        }

        /// <summary>
        /// 通过角色ID获取所有用户ID集合
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public async Task<List<int>> GetUserIdsByRoleId(int roleId)
        {
            return await _userService.GetUserIdsByRoleId(roleId);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Model
{
    public class M_PromotionGoodTitleAndTimeGroup
    {
        public string time { get; set; }

        public string good_title { get; set; }
        public decimal sell_price { get; set; }
        public decimal promotion_price { get; set; }
        public int total_count { get; set; }
        public decimal total_amount { get; set; }
    }
}

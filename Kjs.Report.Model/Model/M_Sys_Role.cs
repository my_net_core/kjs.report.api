﻿using System;

namespace Kjs.Report.Model
{
    /// <summary>
    /// 系统角色类
    /// </summary>
    public partial class M_Sys_Role
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        public int roleId { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        public string roleName { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int? sort { get; set; }
        /// <summary>
        /// 添加人
        /// </summary>
        public int? addUser { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime? addTime { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public int? updateUser { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? updateTime { get; set; }
        /// <summary>
        /// 角色类别
        /// </summary>
        public string roleType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class M_PromotionTimeGroup
    {
        public string time { get; set; }
        public int total_count { get; set; }
        public decimal total_amount { get; set; }
    }
}

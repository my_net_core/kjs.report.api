﻿namespace Kjs.Report.Model.Extensions
{
    public class ApiUrls
    {
        public string JavaApiUrl { get; set; }
        public string JavaRequestKey { get; set; }
        public string AliyunOss { get; set; }
        public string OssEndpoint { get; set; }
        public string OssBucket { get; set; }
        public string OssAccessKey { get; set; }
        public string OssAccessSecret { get; set; }
    }
}

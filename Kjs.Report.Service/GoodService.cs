﻿using Kjs.Report.DB;
using Kjs.Report.IService;
using Kjs.Report.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Service
{
    /// <summary>
    /// 商品报表
    /// </summary>
    public class GoodService : IGoodService
    {
        /// <summary>
        /// 按照商品分组统计在售明细
        /// </summary>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <returns></returns>
        public PagedList<IList<M_GoodSaleGroup>> GoodSaleGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
	a.good_name,
	a.sell_price,
	SUM(a.stock_quantity) stock_quantity,
	(
		a.sell_price * SUM(a.stock_quantity)
	) total_money
FROM
  terminals e 
	LEFT JOIN terminal_good_relation a on e.id=a.terminal_id
WHERE
	stock_quantity > 0
");
            if (!string.IsNullOrWhiteSpace(orgIds))
                strSql.AppendFormat(" and a.org_id in ({0}) ", orgIds);

            if (line_id > 0)
                strSql.AppendFormat(" and e.line_id = {0} ", line_id);

            if (point_id > 0)
                strSql.AppendFormat(" and e.point_id = {0} ", point_id);

            strSql.Append(" group by a.good_name,a.sell_price  ");

            if (string.IsNullOrWhiteSpace(filedOrder))
                filedOrder = " a.sell_price desc ";

            return PagedHelper.PagedList<M_GoodSaleGroup>(strSql.ToString(), pageSize, pageIndex, filedOrder);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Common
{
    public class Utils
    {
        /// <summary>

        /// 得到百分比函数
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns> 格式为0.0%</returns>
        public static string GetPercent(int A, int B)
        {
            double rate = new double();
            try
            {
                rate = Convert.ToDouble(A) / Convert.ToDouble(B);
            }
            catch (Exception ex)
            {
                return null;
            }
            return rate.ToString("p");
        }
    }
}

﻿using AutoMapper;
using Kjs.Report.Domain.Table;
using Kjs.Report.Model;

namespace Kjs.Report.Admin.Mapper
{
    /// <summary>
    /// 用户mapper类
    /// </summary>
    public class SysUserProfile : Profile
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public SysUserProfile()
        {
            CreateMap<M_Sys_User, D_Sys_User>()
                .ForMember(d => d.id, m => m.MapFrom(x => x.userId))
                .ForMember(d => d.user_name, m => m.MapFrom(x => x.userName))
                .ForMember(d => d.user_no, m => m.MapFrom(x => x.userNo))
                .ForMember(d => d.real_name, m => m.MapFrom(x => x.realName))
                .ForMember(d => d.role_id, m => m.MapFrom(x => x.roleId))
                .ForMember(d => d.password, m => m.MapFrom(x => x.password))
                .ForMember(d => d.phone, m => m.MapFrom(x => x.phone))
                .ForMember(d => d.is_enable, m => m.MapFrom(x => x.isEnable))
                .ForMember(d => d.is_super, m => m.MapFrom(x => x.isSuper))
                .ForMember(d => d.head_img, m => m.MapFrom(x => x.headImg))
                .ForMember(d => d.add_user, m => m.MapFrom(x => x.addUser))
                .ForMember(d => d.add_time, m => m.MapFrom(x => x.addTime))
                .ForMember(d => d.update_user, m => m.MapFrom(x => x.updateUser))
                .ForMember(d => d.update_time, m => m.MapFrom(x => x.updateTime))
                .ForMember(d => d.last_login_ip, m => m.MapFrom(x => x.lastLoginIp))
                .ForMember(d => d.email, m => m.MapFrom(x => x.email))
                .ForMember(d => d.last_login_time, m => m.MapFrom(x => x.lastLoginTime));

            CreateMap<D_Sys_User, M_Sys_User>()
                .ForMember(m => m.userId, d => d.MapFrom(x => x.id))
                .ForMember(m => m.userName, d => d.MapFrom(x => x.user_name))
                .ForMember(m => m.userNo, d => d.MapFrom(x => x.user_no))
                .ForMember(m => m.realName, d => d.MapFrom(x => x.real_name))
                .ForMember(m => m.roleId, d => d.MapFrom(x => x.role_id))
                .ForMember(m => m.password, d => d.MapFrom(x => x.password))
                .ForMember(m => m.phone, d => d.MapFrom(x => x.phone))
                .ForMember(m => m.isEnable, d => d.MapFrom(x => x.is_enable))
                .ForMember(m => m.isSuper, d => d.MapFrom(x => x.is_super))
                .ForMember(m => m.headImg, d => d.MapFrom(x => x.head_img))
                .ForMember(m => m.addUser, d => d.MapFrom(x => x.add_user))
                .ForMember(m => m.addTime, d => d.MapFrom(x => x.add_time))
                .ForMember(m => m.updateUser, d => d.MapFrom(x => x.update_user))
                .ForMember(m => m.updateTime, d => d.MapFrom(x => x.update_time))
                .ForMember(m => m.lastLoginIp, d => d.MapFrom(x => x.last_login_ip))
                .ForMember(d => d.email, m => m.MapFrom(x => x.email))
                .ForMember(m => m.lastLoginTime, d => d.MapFrom(x => x.last_login_time));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Kjs.Report.IService;
using Microsoft.AspNetCore.Authorization;

namespace Kjs.Report.Admin.Controllers
{
    /// <summary>
    /// 订单统计
    /// </summary>
    [Produces("application/json")]
    [Authorize("Bearer")]
    [EnableCors("Any")]
    [Route("api/promotiongood")]
    public class PromotionGoodController : ApiControllerBase
    {
        private IPromotionGoodService _promotionGoodService;
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="promotionGoodService"></param>
        public PromotionGoodController(IPromotionGoodService promotionGoodService)
        {
            _promotionGoodService = promotionGoodService;
        }

        /// <summary>
        /// 按照商品名称分组统计销售数据明细
        /// </summary>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet("sale/goodgroup/page/list")]
        public object GoodNameGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int line_id = 0, int point_id = 0, DateTime? beginTime = null,
            DateTime? endTime = null)
        {
            return _promotionGoodService.GoodNameGroupList(pageSize, pageIndex, "", orgIds, line_id, point_id, beginTime, endTime);
        }

        /// <summary>
        /// 按照商品名称分组统计销售数据明细
        /// </summary>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet("sale/timegroup/page/list")]
        public object TimeGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int line_id = 0, int point_id = 0, DateTime? beginTime = null,
            DateTime? endTime = null)
        {
            return _promotionGoodService.TimeGroupList(pageSize, pageIndex, "", orgIds, line_id, point_id, beginTime, endTime);
        }

        /// <summary>
        /// 按照时间及商品名称分组统计销售数据明细
        /// </summary>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet("sale/titleandtimegroup/page/list")]
        public object TitleAndTimeGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int line_id = 0, int point_id = 0, DateTime? beginTime = null,
            DateTime? endTime = null)
        {
            return _promotionGoodService.TitleAndTimeGroupList(pageSize, pageIndex, "", orgIds, line_id, point_id, beginTime, endTime);
        }
    }
}

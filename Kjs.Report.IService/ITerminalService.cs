﻿using Kjs.Report.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.IService
{
    public interface ITerminalService
    {

        /// <summary>
        /// 按照时间分组统计终端明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="filedOrder">排序字段</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="province_id">省编号</param>
        /// <param name="city_id">市编号</param>
        /// <param name="area_id">区编号</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        PagedList<IList<M_TerminalTimeGroup>> TimeGroupList(int pageSize, int pageIndex, string filedOrder, int province_id, int city_id, int area_id, string orgIds,
            int line_id, int point_id, DateTime? beginTime, DateTime? endTime);
    }
}

﻿namespace Kjs.Report.JWT
{
    public class JwtPermission
    {
        /// <summary>
        /// 角色凭据名称
        /// </summary>
        public virtual string RoleName
        { get; set; }
        /// <summary>
        /// 模块数组
        /// </summary>
        public virtual string [] ModuleArryList
        { get; set; }
    }
}
